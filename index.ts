import http from 'http'
import path from 'path'
import express from 'express'
import { Server } from 'socket.io'
const app = express()
const server = http.createServer(app)
const io = new Server(server)

app.use(express.static('public'))

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'))
})

io.on('connection', (socket) => {
  socket.broadcast.emit('hi')
  let username = 'anonymous'

  socket.on('chat message', (message) => {
    io.emit('chat message', { username, message })
  })

  const usersList = []
  socket.on('register username', (newUsername) => {
    username = newUsername
    usersList.push(username)
    io.emit('usersList', usersList)
  })
  // io.emit('chat message', 'user connected')
  // socket.on('disconnect', () => {
  //   io.emit('chat message', 'user disconnected')
  // })
  // socket.on('chat message', (msg) => {
  //   io.emit('chat message', `${msg} - ${date()}`)
  // })
})

io.emit('some event', { someProperty: 'some value', otherProperty: 'other value' }) // This will emit the event to all connected sockets

server.listen(3000, () => {
  console.log('listening on *:3000')
})
